import React from "react";
import {
    Container,
    Grid,
    Paper,
    TextField,
    Button,
    Typography,
} from "@material-ui/core";
import { ErrorMessage } from "@hookform/error-message";
import { useForm } from "react-hook-form";
import { Send } from "@material-ui/icons";
import { useDispatch } from "react-redux";
import { SAVE_INFO } from "../store/reducers";

export default function LoginPage() {
    const { register, handleSubmit, errors } = useForm();
    const dispatch = useDispatch()
    const onSubmit = async (data) => {

        if (data.username === "demo" && data.password === "123456") {
            dispatch(SAVE_INFO(data))
            return alert("Login thành công");
        }
        return alert("Thông tin đăng nhập chưa chính xác")
    };
    return (
        <Container
            maxWidth="sm"
            style={{
                height: "100vh",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
            }}
        >
            <Grid spacing={3} container>
                <Grid xs={12} item>
                    <Paper>
                        <div style={{ padding: "12px 24px 0px 24px" }}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <Grid container spacing={2} style={{ padding: 24 }}>
                                    <Grid item xs={12} style={{ textAlign: "center" }}>
                                        <Typography style={{ marginBottom: 16 }} variant="h6">
                                            SIGN IN"
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <TextField
                                            error={errors.username}
                                            fullWidth
                                            helperText={
                                                <ErrorMessage errors={errors} name="username">
                                                    {({ messages }) =>
                                                        messages &&
                                                        Object.entries(messages).map(([type, message]) => (
                                                            <p key={type}>{message}</p>
                                                        ))
                                                    }
                                                </ErrorMessage>
                                            }
                                            id="outlined-basic"
                                            required
                                            label="Username"
                                            name="username"
                                            variant="outlined"
                                            inputRef={register({
                                                required: "Please enter a username",
                                            })}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            error={errors.password}
                                            fullWidth
                                            helperText={
                                                <ErrorMessage errors={errors} name="password">
                                                    {({ messages }) =>
                                                        messages &&
                                                        Object.entries(messages).map(([type, message]) => (
                                                            <p key={type}>{message}</p>
                                                        ))
                                                    }
                                                </ErrorMessage>
                                            }
                                            id="outlined-basic"
                                            required
                                            type="password"
                                            label="Your Password"
                                            name="password"
                                            variant="outlined"
                                            inputRef={register({
                                                required: "Please enter your password !",
                                            })}
                                        />
                                    </Grid>

                                    <Grid item xs={12} style={{ textAlign: "center" }}>
                                        <Button
                                            size="large"
                                            variant="contained"
                                            type="submit"
                                            color="primary"
                                            endIcon={<Send />}
                                        >
                                            Login
                                        </Button>
                                    </Grid>
                                </Grid>
                            </form>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
        </Container>
    );
}
