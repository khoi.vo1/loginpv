import { createSlice } from "@reduxjs/toolkit";

const initialStore = {
  username: "",
  password: "",
};
const storeReducers = createSlice({
  name: "store",
  initialState: initialStore,
  reducers: {
    SAVE_INFO: (state, action) => {
      state.username = action.payload.username;
      state.password = action.payload.password;
    },

  },
  extraReducers: {},
});
const { reducer, actions } = storeReducers;

export const { SAVE_INFO } = actions;

export default reducer;
